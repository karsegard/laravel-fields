<?php

namespace KDA\Fields\View\Components;

use Illuminate\View\Component;

class Select2 extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $placeholder ;
    public $processResultFunction;
    public $ajax;
    public $url;
    public function __construct($placeholder="Search",$processResultFunction=NULL,$ajax=false,$url='')
    {
        $this->placeholder=$placeholder;
        $this->processResultFunction = $processResultFunction;
        $this->ajax=  $ajax;
        $this->url = $url;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('kda-fields::components.select2');
    }
}
