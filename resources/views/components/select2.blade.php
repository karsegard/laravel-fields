    <select data-placeholder="{{ $placeholder }}" class="form-control input-group-lg" {{ $attributes }}
        data-init-function="initKDAFieldSelect2" style="width:100%; height:55px;" data-ajax="{{$ajax}}"
        data-ajax_url="{{$url}}">

    </select>

    @push('kda_fields_styles')
        @once
            <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
                rel="stylesheet" />


        @endonce
    @endpush

    @push('kda_fields_scripts')
        @once
            <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
        @endonce
        @once
            <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js" defer></script>
        @endonce

        <script>
            function initKDAFieldSelect2(element) {
                let ajax = element.data('ajax');

                let ajax_settings = {};
                if (ajax) {
                    let ajax_url = element.data('ajax_url');
                    debugger;
                    ajax_settings = {
                        ajax: {
                            url: ajax_url,
                            dataType: 'json',
                            type: "GET",
                            delay: 500,

                            @if ($processResultFunction)
                                processResults: {!! $processResultFunction !!}
                            @endif
                        }
                    }


                }

                element.select2({
                    theme: "bootstrap",
                    minimumInputLength: 2,
                    ...ajax_settings
                });
            }
        </script>
    @endpush
